# View Android version

Simple app to view Android version information. Usually these information are visible under `Settings` => `About`, but on some ROMs, e. g. FireOS they are not visible.

## Download
[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/de/packages/com.github.mueller_ma.viewandroidversion/)

Debug version of the current master available via GitLab. Main repo page => Download icon => Artifacts => Download 'build'

## Screenshot

<img src="fastlane/metadata/android/en-US/phoneScreenshots/1.png" alt="Screenshot" height="800">


## License
[GNU GPL-3](https://gitlab.com/mueller-ma/View-android-version/blob/master/LICENSE)

Android is a trademark of Google LLC.
